/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorsocket;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ThreadCliente extends Thread {

    //Criando um atributo do tipo Socket
    private Socket cliente;

    //Método responsável por receber iniciar as threads de cada conexão cliente
    public ThreadCliente(Socket cliente) {
        this.cliente = cliente;
        executarServidor();
    }

    //Método responsável por enviar as mensagens para os clientes socket
    public void executarServidor() {
        try {
            //Instanciando o objeto responsável por enviar a mensagem via Socket
            ObjectOutputStream saida = new ObjectOutputStream(cliente.getOutputStream());
            saida.flush();
            //Definido a mensagem de forma estática a ser enviada via socket
            String texto = "MENSAGEM BEM SUCEDIDA!";
            //Atribindo a mensagem ao objeto responsável por enviar a mensagem via socket
            saida.writeUTF(texto);
            //Finalizando as instancias do envio da mensagem e thread da comunicação server socket
            saida.close();
            cliente.close();
        } catch (Exception e) {
            // Está mensagem será exibida via console, caso ocorrer algum erro com a thread de comunicação
            System.out.println("Excecao ocorrida na thread: " + e.getMessage());
        }
    }
}
