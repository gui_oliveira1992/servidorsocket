package servidorsocket;

import java.net.ServerSocket;
import java.net.Socket;

public class ServidorSocket {

    public static void main(String[] args) {

        try {
            //Atribuindo/Definindo o número da porta de comunicação padrão do servidor  
            int port = 1050;

            System.out.println("Incializando o servidor...");
            
            //Iniciliza o servidor
            ServerSocket serv = new ServerSocket(port);

            System.out.println("Servidor iniciado, ouvindo a porta " + port);
            //Aguarda conexões
            while (true) {
                Socket cliente = serv.accept();
                //Inicia thread do cliente
                new ThreadCliente(cliente).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
